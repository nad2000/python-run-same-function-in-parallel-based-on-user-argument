#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ai ts=4 sts=4 et sw=4 ft=python

"""
Example showing how to run multirunner
"""

from multirunner import run
import time
import logging

logging.basicConfig(level=logging.DEBUG)


def run_fst(fst_in, fst_logfile, system_name, user_name, user_password):
    print("*** Running 'run_fst' with parameters:", fst_in, fst_logfile, system_name, user_name, user_password)
    time.sleep(1)
    return (False, 0)

# Execute once
run(run_fst, fst_in="FST_IN", fst_logfile="/tmp/job.out",
        system_name="SYSTEM_NAME", user_name="USER_NAME",
        user_password="USER_PASSWORD")

