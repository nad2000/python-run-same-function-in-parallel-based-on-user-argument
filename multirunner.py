#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ai ts=4 sts=4 et sw=4 ft=python

import os
import sys
import multiprocessing
import logging

def run(func, fst_in, fst_logfile, system_name, user_name, user_password,
        num_of_jobs=1):
    """
    Executes in paralled func with passed arguments
    and handles the results
    """

    def log_name(i):
        """Creates a unique logfile name with 'i' sequence number."""
        if num_of_jobs==1:
            # if running a single process don't change the name:
            return fst_logfile
        else:
            return str(i).join(os.path.splitext(fst_logfile))

    pool = multiprocessing.Pool(processes=num_of_jobs)
    results = pool.starmap(func, [(fst_in, 
        log_name(i), system_name, user_name,
        user_password) for i in range(1, num_of_jobs+1)])
    
    this_pass = this_fail = 0

    for i, (fst_run_result, fst_errors_that_not_ignore) in enumerate(results, 1):
        
        if not fst_run_result:
            logging.info("It Passed")
            this_pass = this_pass + 1
        else:
            logging.error(
                    "Error found that can not be ignored: %d, "
                    "please check out this log file: %s",
                    fst_errors_that_not_ignore, log_name(i))
            this_fail = this_fail + 1


    if this_fail > 0:
        logging.error("There is %s failed, out of %s jobs",
                this_fail, num_of_jobs)
        sys.exit(1)

    else:
        logging.info("Test Passed")
        sys.exit(0)

